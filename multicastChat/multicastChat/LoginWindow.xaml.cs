﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace multicastChat
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {

        private string loginNick;
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void OKbutton_Click(object sender, RoutedEventArgs e)
        {
            if (!textBox.Text.Equals(""))
            {
                loginNick = textBox.Text;
                this.DialogResult = true;
            }
            
        }

        private void Cancelbutton1_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        public string getNick()
        {
            return loginNick;
        }
    }
}
