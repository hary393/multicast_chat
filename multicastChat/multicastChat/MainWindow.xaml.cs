﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Windows.Threading;

namespace multicastChat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Reciver odbiornik = new Reciver();

        Socket s = new Socket(AddressFamily.InterNetwork,  //socket for sending stuff out
                SocketType.Dgram, ProtocolType.Udp);

        Thread workerThread;   //thread for reciver

        string loginNick;
        List<string> userList = new List<string>();

        public MainWindow()
        {
            InitializeComponent();
            LoginWindow loginWindow = new LoginWindow();
            var result = loginWindow.ShowDialog();
            if (result == true)
            {
                
                loginNick = loginWindow.getNick();
                userList.Add(loginNick);
                userBox2.Text = "User List\n" + string.Join(" \n", userList);

                Action ExplicitDelegate = ReturnNameMessage;

                workerThread = new Thread(() => odbiornik.reciveData(chatBox1,userBox2,userList,ExplicitDelegate));
                workerThread.Start();

                IPAddress ip = IPAddress.Parse("224.5.6.7");//Multicast IP addresses are within the Class D range of 224.0.0.0-239.255.255.255
                try
                {
                    s.SetSocketOption(SocketOptionLevel.IP,
                                      SocketOptionName.AddMembership, new MulticastOption(ip));
                    s.SetSocketOption(SocketOptionLevel.IP,
                    SocketOptionName.MulticastTimeToLive, 18);

                    IPEndPoint ipep = new IPEndPoint(ip, 4567);
                    s.Connect(ipep);

                    Ramka b = new Ramka();  //message indicating new user
                    b.type = MessageType.AddName;
                    b.who = loginNick;
                    Message wiad = Serializer.Serialize(b);
                    s.Send(wiad.Data, wiad.Data.Length, SocketFlags.None);

                    DispatcherTimer timer = new DispatcherTimer();
                    timer.Interval = TimeSpan.FromMinutes(5);
                    timer.Tick += RebrodcastNames;
                    timer.Start();
                }
                catch (Exception)
                {
                    MessageBox.Show("There was a problem tryung to connect. If this problem persists please restart your pc", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    Application.Current.Shutdown();
                }
                
            }
            else
            {
                Application.Current.Shutdown();
            }

        }

        private void RebrodcastNames(object sender, EventArgs e) //i case somebody crashes and his name lingers in chat
        {
            userList.Clear();
            userList.Add(loginNick);
            userBox2.Text = "User List\n" + string.Join(" \n", userList);
            Ramka b = new Ramka();
            b.type = MessageType.Rebrodcast;
            b.who = loginNick;
            Message wiad = Serializer.Serialize(b);
            s.Send(wiad.Data, wiad.Data.Length, SocketFlags.None);
        }

        private void ReturnNameMessage()  //message brodcasting that youre in chat
        {
            Ramka b = new Ramka();
            b.type = MessageType.AnswerName;
            b.who = loginNick;
            Message wiad = Serializer.Serialize(b);
            s.Send(wiad.Data, wiad.Data.Length, SocketFlags.None);
        }
        private void DeleteNameMessage() // message send to delete name from chat (used when leaving)
        {
            Ramka b = new Ramka();
            b.type = MessageType.DeleteName;
            b.who = loginNick;
            Message wiad = Serializer.Serialize(b);
            s.Send(wiad.Data, wiad.Data.Length, SocketFlags.None);
        }
        protected override void OnClosed(EventArgs e) //handling threads on close
        {
            base.OnClosed(e);
            if (workerThread !=null)  //in case you closed before picking name
            {
                this.DeleteNameMessage();
                s.Close();
                odbiornik.StopThread();
                workerThread.Join();
            }
            
        }
        /// <summary>
        /// custom comand functions
        /// </summary>

        private void SendCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void SendCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Ramka b = new Ramka();
            b.type = MessageType.SendMessage;
            b.send = DateTime.Now;
            b.who = loginNick;
            b.text = inputBox.Text;
            Message wiad = Serializer.Serialize(b);
            s.Send(wiad.Data, wiad.Data.Length, SocketFlags.None);
            inputBox.Text = "";
        }
    }


    class Reciver  // class handling reciver socket 
    {
        public Reciver() { }
        public Boolean working = true;
        Socket soc;
        public void reciveData(TextBox Box, TextBox userBox, List<string>listOfUsers, Action sendUpdate)// textbox for chat, textbox for user list, userList list and delegate for return messages
        {
            soc = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
             ProtocolType.Udp);
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 4567);
            soc.Bind(ipep);

            IPAddress ip = IPAddress.Parse("224.5.6.7");

            soc.SetSocketOption(SocketOptionLevel.IP,
                SocketOptionName.AddMembership,
                    new MulticastOption(ip, IPAddress.Any));
            byte[] b = new byte[1024];
            sendUpdate.Invoke();

            while (working)
            {
                try
                {
                    soc.Receive(b);
                    Message wiad = new Message();
                    wiad.Data = b;
                    Ramka obj = (Ramka)Serializer.Deserialize(wiad);

                    /////// Handling diffrent message types
                    if (obj.type.Equals(MessageType.SendMessage)) // Normal Message
                    {
                        Box.Dispatcher.Invoke(() =>
                        {
                            Box.AppendText("[" + obj.send.ToLongTimeString() + "]");
                            Box.AppendText(obj.who+": ");
                            Box.AppendText(obj.text + "\n");
                            Box.ScrollToEnd();
                        });
                    }
                    else if(obj.type.Equals(MessageType.AddName)) // New User Message
                    {
                        listOfUsers.Add(obj.who);
                        userBox.Dispatcher.Invoke(() =>
                        {
                            userBox.Text = "User List\n" + string.Join(" \n", listOfUsers);
                        });
                        Box.Dispatcher.Invoke(() =>
                        {
                            Box.AppendText(obj.who + " joined the chat \n");
                            Box.ScrollToEnd();
                        });
                        sendUpdate.Invoke();  // inform main thread to send return message
                    }
                    else if (obj.type.Equals(MessageType.AnswerName)) // Brodcasting names add to your list of users
                    {
                        if (!listOfUsers.Contains(obj.who))
                        {
                            listOfUsers.Add(obj.who);
                            userBox.Dispatcher.Invoke(() =>
                            {
                                userBox.Text = "User List\n" + string.Join(" \n", listOfUsers);
                            });
                        }
                    }
                    else if (obj.type.Equals(MessageType.DeleteName))    // Message that somebody left 
                    {
                        if (listOfUsers.Contains(obj.who) && obj.who!=listOfUsers[0]) //listOfUsers[0]== you
                        {
                            listOfUsers.Remove(obj.who);
                            userBox.Dispatcher.Invoke(() =>
                            {
                                userBox.Text = "User List\n" + string.Join(" \n", listOfUsers);
                            });
                            Box.Dispatcher.Invoke(() =>
                            {
                                Box.AppendText(obj.who + " left the chat \n");
                                Box.ScrollToEnd();
                            });
                        }
                    }
                    else if (obj.type.Equals(MessageType.Rebrodcast)) // Just send your name
                    {
                        sendUpdate.Invoke();
                    }


                }
                catch (Exception)
                {
                    //i know im missing exception on purpose
                    //but i have no idea how to force socket close otherwise
                }
            }
        }

        public void StopThread()
        {
            working = false;
                soc.Close();
        }
    }
    /// <summary>
    /// To send Frames throught sockets they need to be serialized so here are prototypes and functions
    /// </summary>
    public class Message
    {
        public byte[] Data { get; set; }
    }
    public static class Serializer
    {

        public static Message Serialize(object anySerializableObject)
        {
            using (var memoryStream = new MemoryStream())
            {
                (new BinaryFormatter()).Serialize(memoryStream, anySerializableObject);
                return new Message { Data = memoryStream.ToArray() };
            }
        }

        public static object Deserialize(Message message)
        {
            using (var memoryStream = new MemoryStream(message.Data))
                return (new BinaryFormatter()).Deserialize(memoryStream);
        }

    }

    public enum MessageType
    {
        SendMessage, AddName, AnswerName, DeleteName, Rebrodcast
    }
    [SerializableAttribute]
    public struct Ramka
    {
        public MessageType type;
        public string text;
        public string who;
        public DateTime send;
    }


    public static class CustomCommands
    {
        public static readonly RoutedUICommand Send = new RoutedUICommand
                (
                        "Send",
                        "Send",
                        typeof(CustomCommands),
                        new InputGestureCollection()
                        {
                                        new KeyGesture(Key.Enter)
                        }
                );
    }

}
